# Intro

This is the repo were all the code && information will be used for our
purposes :rocket: :cat:

## MUST HAVE!!

- [Ngrok](https://ngrok.com/download)
- [java](https://www.java.com/en/download/help/download_options.html) (or npm :eyes:) 
- IDE of your choice

## Description:

Slack has made a framework called [BOLT](https://api.slack.com/tools/bolt)
which is a family of CDKs that can be used to integrate any piece of code to
suit your needs :) Check that as an entrypoint! 



environmental variable (specific to the app so if you create a new one you will need to change them)
`SLACK_BOT_TOKEN=xoxb-84169475937-3689208186610-UVTvz6MSoCb5GxzIdEUhCYgR;SLACK_SIGNING_SECRET=101dcce5472e7c0722d7c87a8d54bd42`
You need to use ngrok to run it locally : `ngrok http 4390`

You can see the victoria UAT user pinned in the #hackathon-octopus channel

## Links of interest
- [tutorial](https://api.slack.com/tutorials/tags/dotnet) for .NET
- [tutorial](https://api.slack.com/tutorials/tags/java) for Java
- [tutorial](https://slack.dev/java-slack-sdk/guides/getting-started-with-bolt) with Spring Boot
- [tutorial](https://api.slack.com/tutorials/slash-block-kit), [tut2](https://slack.dev/bolt-js/tutorial/getting-started) for nodejs
- [link](https://api.slack.com/apps) to see the app I have set up (maybe you can see it since we are in the same workspace not sure you though) otherwise it shouldn't be too hard to create one through the slack interface
- to understand more about [slash commands](https://api.slack.com/interactivity/slash-commands)

