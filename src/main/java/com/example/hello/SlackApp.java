package com.example.hello;
import com.slack.api.bolt.App;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.ArrayList;



@Configuration
public class SlackApp {
    @Bean
    public App initSlackApp() {
        App app = new App();
        app.command("/checkusers", (req, ctx) -> {
            ArrayList<String> list = new ArrayList<>();
            String username = req.getPayload().getUserName();
            var subjects = UserManagement.getInstance().getUsersIamInChargeOf(username);
            subjects.forEach(t ->
            {
                var password = CredentialsProvider.getInstance().getPassword(t);
                var token = AuthHelper.GetBearerToken(t, password);
                String url = "https://app.dcslsoftware.com/VictoriaUAT/api/v1/timesheet?date=2022-06-13";//+DateUtils.getCurrentDate();
                var response = RestClient.GetRequest(url, token);
                var userDone = TimesheetReader.isUserDone(response);
                if (userDone == false)
                {
                    list.add(":x: " + t + " please fill timesheet");
                }
                else
                {
                    list.add(":white_check_mark: " + t + " good job!");
                }
            });

            var result = "";

            for (int i = 0; i < list.size(); i++) {
                result = result + ("\n" + list.get(i) + "\n");
            }

            return ctx.ack(":wave: Hello " + username + " this is the current timesheet status\n"+ result);
        });
        return app;
    }
}
