package com.example.hello;

import java.util.*;

public class UserManagement {

    private Map<String, List<String>> managementRelationships;

    private static UserManagement userManagement;

    private UserManagement(Map<String, List<String>> managementRelationships){
        this.managementRelationships = managementRelationships;
    }

    public static UserManagement getInstance(){
        if (userManagement == null){
            return new UserManagement(createManagementRelationships());
        }else{
            return userManagement;
        }
    }

    private static Map<String,List<String>> createManagementRelationships(){
        Map<String,List<String>> managementRelationships = new HashMap<>();
        managementRelationships.put("malik.bekkouche", Arrays.asList("Dauda.Kadiri@DCSLSoftware.com","Malik.Bekkouche@dcsl.com"));
        managementRelationships.put("dauda.kadiri",Arrays.asList("Malik.Bekkouche@dcsl.com","Dauda.Kadiri@DCSLSoftware.com"));
        return managementRelationships;
    }

    public List<String> getUsersIamInChargeOf(String username){
        return managementRelationships.get(username);
    }



}
