package com.example.hello;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class RestClient {

    public static String PostRequest(String url, Map<String, String> map, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();
        Object requestObject = null;
        if (accessToken != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer " + accessToken);
            requestObject = new HttpEntity<Object>(map, headers);
        } else {
            requestObject = map;
        }
        ResponseEntity<String> response = restTemplate.postForEntity(url, requestObject, String.class);
        var body = response.getBody();
        return body;
    }

    public static String GetRequest(String url, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + accessToken);
        var entity = new HttpEntity<Object>(headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        var body = response.getBody();
        return body;
    }
}
