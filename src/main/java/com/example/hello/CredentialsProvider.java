package com.example.hello;

import java.util.HashMap;
import java.util.Map;

public class CredentialsProvider {

    private CredentialsProvider(Map<String, String> credentials) {
        this.credentials = credentials;
    }

    private  Map<String,String> credentials;
    private static CredentialsProvider instance;

    public static CredentialsProvider getInstance(){
        if(instance == null){
            return new CredentialsProvider(createCredentialsMap());
        }else{
            return instance;
        }
    }

    private static Map<String,String> createCredentialsMap(){
        Map<String,String> credentialsMap = new HashMap<>();
        credentialsMap.put("Malik.Bekkouche@dcsl.com","passwordUATtest1!");
        credentialsMap.put("Dauda.Kadiri@DCSLSoftware.com","octopusUAT!Test1");
        return credentialsMap;
    }

    public String getPassword(String username){
        return credentials.get(username);
    }
}
