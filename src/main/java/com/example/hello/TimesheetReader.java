package com.example.hello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TimesheetReader {

    public static boolean isUserDone(String response){
        try {
            JsonNode ob = new ObjectMapper().readValue(response, JsonNode.class);
            var data = ob.get("data");
            var timeWorked = data.get("recordedTime").asInt();
            var requiredTime = data.get("requiredTime").asInt();
            return timeWorked >= requiredTime;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }
}
