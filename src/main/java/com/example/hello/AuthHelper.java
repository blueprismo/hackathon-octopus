package com.example.hello;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class AuthHelper {
    public static String GetBearerToken(String user, String password) {
        String authUrl = "https://app.dcslsoftware.com/VictoriaUAT/api/v1/authorisation";
        Map<String, String> map = new HashMap<>();
        map.put("username", user);
        map.put("password", password);
        var response = RestClient.PostRequest(authUrl, map, null);
        try {
            JsonNode ob = new ObjectMapper().readValue(response, JsonNode.class);
            return ob.get("data").asText();

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
